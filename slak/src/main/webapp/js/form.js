//const form_uri___ = host_base + "/rest/json/";


const my_test_uri = "rest/messages/mytest/";
const form_uri = "rest/json/";


function on_form_response(v) {
  $('#result').text(v);
}

function request_form_response(name, cb) {
  $.ajax({
      url: form_uri + name,
      dataType: 'json',
      data: {},
      type: 'GET',
      success: function(response) {
          cb(response.result);
      },
        error: function(err) {
          cb(err.status +": "+err.statusText +" ("+form_uri+")");
      }
    });
}


function request_get(uri, cb) {
  $.ajax({
      url: uri,
      dataType: 'json',
      data: {},
      type: 'GET',
      success: function(response) {
          cb(response.data);
      },
        error: function(err) {
          cb(err.status +": "+err.statusText +" ("+uri+")");
      }
    });
}

function hello_form_on_click() {
  $('#sayHello').click(function(event) {
    event.preventDefault();
    var result = $('#result'),
    name = $.trim($('#name').val());
    result.removeClass('invalid');
    if (!name || !name.length) {
      result.addClass('invalid').text('A name is required!');
      return;
    }
    request_form_response(name, on_form_response);
  })
}

function hello_populate_on_click() {
  $('#populate').click(function(event) {
    event.preventDefault();
    var result = $('#result_pop'),
    id = $.trim($('#name').val());
    result.removeClass('invalid');
    if (!id || !id.length) {
      result.addClass('invalid').text('An id is required!');
      return;
    }
    request_get(my_test_uri+id, on_form_response);
  })
}

function init_form_page() {
   getNavbar("home", on_insert_navbar);
   hello_form_on_click();
   hello_populate_on_click();
}

(function() {

//

})();