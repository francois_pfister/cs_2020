

/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for Message  --------------*/

var message_example = {
  id:999,
  content: "Foo"
}

var current_message;
var current_user;



function log_message(m) {
  $('#messageMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_message(m) {
  $('#messageMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}

function handle_message_error(error, mesg) {
  if ((error.status == 409) || (error.status == 400)) {
    error_message(mesg);
    var errorMsg = $.parseJSON(error.responseText);
    $.each(errorMsg, function(index, err) {
      document.getElementById("msg_" + index).innerHTML = err;
    });
  } else {
    error_message(error.status + ": " + error.statusText);
  }
}

function handle_message_success(message, mesg) {
  current_message = message;
  $('#message_form')[0].reset();
  log_message(mesg);
  request_messages();
}

function handle_message_list_success(messages) {
    $("#messagetable").empty().append(update_message_view(messages));
}

function on_success_message_update(message) {
  handle_message_success(message, "Message Updated");
}

function on_success_message_add(message) {
  handle_message_success(message, "Message Added");
}

function on_success_message_delete_byid(message) {
  handle_message_success(undefined, "Message Deleted");
}

function on_error_message_delete_all(error) {
  handle_message_error(error,"error while deleting all Messages!");
}

function on_error_message_delete_byid(error) {
 handle_message_error(error,"error while deleting Message!");
}

function on_error_message_update(error) {
  handle_message_error(error,"error while updating Message!");
}

function on_error_message_add(error) {
  handle_message_error(error,"error while adding Message!");
}

function on_error_messages(error) {
     error_message("error request Messages -" + error.status);
}

function request_messages() {
  request_entities(messages_uri+"/user/"+current_user.id, handle_message_list_success, on_error_messages);
}

function update_message_view(messages) {
  if (current_message == undefined)
    current_message = messages[0]
  update_message_form(current_message);
  return _.template($("#message-template").html(), {
    "messages": messages
  });
}

function on_success_message_delete_all(message) {
  current_message = undefined;
  $('#message_form')[0].reset();
  log_message("All Messages Deleted");
  request_messages();
}

function update_message_form(message) {
  try {
    //$('#name').val(message.name);
    //$('#email').val(message.email);
    
    log_message(JSON.stringify(message));
    
    
    current_message = message;
    $('#message_form').values(message);
    if (message.channel != undefined)
      $('#channel').val(message.channel.name);    //when not editable with a select box
    
    if (message.emitter != undefined)
      $('#emitter').val(message.emitter.name);  //when not editable with a select box
    
    
  } catch (error) {
    error_message("error update_message_form");
  }
}

function on_error_message_form(err) {
  error_message(err.status + ": " + err.statusText);
}

function message_find(id) {
  request_entity(messages_uri + "/" + id,update_message_form, on_error_message_form);
}

function message_delete(id) {
  delete_by_id(messages_uri + "/" + id, on_success_message_delete_byid, on_error_message_delete_byid);
}

function clear_message_errors() {
  $('#messagelayout').find("span.form_err").each(function(index) {
    $(this)[0].innerHTML = "";
  });
}

function send_message(entity) {
	$.ajax({
		url : messages_uri,
		contentType : "application/json",
		dataType : "json",
		type : "PUT",
		data : JSON.stringify(entity),
		success : function(response) {
			log_message(response);
		},
		error : function(err) {
			error_message(err.responseText);
		}
	});
}


function message_test(uri) {
	$.ajax({
		url : uri,
		type : "POST",
		success : function(response) {
			log_message(response);
		},
		error : function(err) {
			error_message(err);
		}
	});
}


function init_message_page() {
  log_message("___init_message_page___");
  clear_message_errors();
  current_user =  JSON.parse(window.sessionStorage.getItem('current_user'));
  $('#from_user_id').val(current_user.name);
    

  document.getElementById("legend_message").textContent = "Messages by user "+current_user.name;
  

  document.getElementById("message_json_href").href = messages_uri+"/user/"+current_user.id;
  document.getElementById("message_json_label").textContent = "REST URL for messages by "+current_user.name;
  
  document.getElementById("back_message").href = window.sessionStorage.getItem('back_navigation')+".html";
  document.getElementById("back_message").innerHTML = "back to "+window.sessionStorage.getItem('back_navigation');



  $('#message_form').submit(function(event) {
    var btn = document.activeElement.getAttribute('id');
    if (btn == "add")
      $('#id').val(-1);
      
    log_message("___message___" + btn);
    event.preventDefault();
    var messageData = $(this).serializeMessage();
    log_message("submit message : " + JSON.stringify(messageData));
    clear_message_errors();
    if (btn == "add")
      update_entity(messages_uri, "POST", messageData, on_success_message_add, on_error_message_add);
    else if (btn == "update")
      update_entity(messages_uri, "PUT", messageData, on_success_message_update, on_error_message_update);
     else if (btn == "send")
      send_message(messageData);     
  });

  $("#message_refresh_button").click(function(event) {
    log_message("___refresh___");
    request_messages();
  });

  $("#message_delete_button").click(function(event) {
    log_message("___delete___all");
    delete_all(messages_uri + "/all", on_success_message_delete_all, on_error_message_delete_all);
  });
  
  $("#message_test_button").click(function(event) {
    log_message("___test___");
    message_test("test7845");
  });  

  $.fn.serializeMessage = function() {
    var o = {};
    var form_data = $('#message_form').serializeArray();
    $.each(form_data, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }
  getNavbar("home", on_insert_navbar);
  try {
    request_messages();
  } catch (error) {
    error_message("error request_messages");
  }
}