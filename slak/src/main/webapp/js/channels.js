/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for Channel  --------------*/



var current_channel;


function log_channel(m) {
  $('#channelMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_channel(m) {
  $('#channelMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}

function handle_channel_error(error, mesg) {
  if ((error.status == 409) || (error.status == 400)) {
    error_channel(mesg);
    var errorMsg = $.parseJSON(error.responseText);
    $.each(errorMsg, function(index, err) {
      document.getElementById("msg_" + index).innerHTML = err;
    });
  } else {
    error_channel(error.status + ": " + error.statusText);
  }
}

function handle_channel_success(channel, mesg) {
  current_channel = channel;
  $('#channel_form')[0].reset();
  log_channel(mesg);
  request_channels();
}

function handle_channel_list_success(channels) {
    window.sessionStorage.setItem("channels", JSON.stringify(channels));
    $("#channeltable").empty().append(update_channel_view(channels));
}

function helo_success(response) {
	 log_channel("ok (helo)");
}

function helo_error(error) {
	error_channel("error (helo)");
}

function channel_update(channel) {
  handle_channel_success(channel, "Channel Updated");
}

function channel_add(channel) {
  handle_channel_success(channel, "Channel Added");
}

function channel_delete_byid(channel) {
  handle_channel_success(undefined, "Channel Deleted");
}

function on_error_channel_delete_all(error) {
  handle_channel_error(error,"error while deleting all Channels!");
}

function on_error_channel_delete_byid(error) {
 handle_channel_error(error,"error while deleting Channel!");
}

function on_error_channel_update(error) {
  handle_channel_error(error,"error while updating Channel!");
}

function on_error_channel_add(error) {
  handle_channel_error(error,"error while adding Channel!");
}

function on_error_channels(error) {
     error_channel("error request Channels -" + error.status);
}

function request_channels() {
  request_entities(channels_uri, handle_channel_list_success, on_error_channels);
}

function update_channel_view(channels) {
  if (current_channel == undefined)
    current_channel = channels[0]
  channel_form(current_channel);
  return _.template($("#channel-template").html(), {
    "channels": channels
  });
}

function channel_delete_all(channel) {
  current_channel = undefined;
  $('#channel_form')[0].reset();
  log_channel("All Channels Deleted");
  request_channels();
}

function channel_form(channel) {
  try {
    //$('#name').val(channel.name);
 
    current_channel = channel;
    $('#channel_form').values(channel);
    $('#privat').prop('checked', channel.privat);
  } catch (error) {
    error_channel("error channel_form");
  }
}

function on_error_channel_form(err) {
  error_channel(err.status + ": " + err.statusText);
}

function channel_find(id) {
  request_entity(channels_uri + "/" + id,channel_form, on_error_channel_form);
}

function channel_delete(id) {
  delete_by_id(channels_uri + "/" + id, channel_delete_byid, on_error_channel_delete_byid);
}


function channel_goto_page(channel,location){
   current_channel = channel;
   var clone_channel = Object.assign({}, current_channel);
   clone_channel.messages = {}; //shallow copy only
   window.sessionStorage.setItem('current_channel', JSON.stringify(clone_channel));
   if (location != undefined)
       window.location = location+".html";
}

function on_channel_error_goto(error,target){
  error_channel("error on_goto_"+target);
}



function channel_goto_messages(channel_id) {
   window.sessionStorage.setItem('back_navigation',"channels");
   if (channel_id != undefined)
      request_entity(channels_uri + "/" + channel_id,channel_goto_page, on_channel_error_goto,"message_bchannels");
   else  
      channel_goto_page(current_channel,"message_bchannels");
}


function clear_channel_errors() {
  $('#channellayout').find("span.form_err").each(function(index) {
    $(this)[0].innerHTML = "";
  });
}

function init_channel_page() {
  request_helo(helo_success,helo_error);
  clear_channel_errors();


  $('#channel_form').submit(function(event) {
    var btn = document.activeElement.getAttribute('id');
    if (btn == "add")
      $('#id').val(-1);
    log_channel("___channel___" + btn);
    event.preventDefault();
    var channelData = $(this).serializeChannel();
    channelData.privat =  $('#privat').prop("checked");
    log_channel("submit channel : " + JSON.stringify(channelData));
    clear_channel_errors();
    if (btn == "add")
      update_entity(channels_uri, "POST", channelData, channel_add, on_error_channel_add);
    else
      update_entity(channels_uri, "PUT", channelData, channel_update, on_error_channel_update);
  });

  $("#channel_refresh_button").click(function(event) {
    log_channel("___refresh___");
    request_channels();
  });

  $("#channel_delete_button").click(function(event) {
    log_channel("___delete___all");
    delete_all(channels_uri + "/all", channel_delete_all, on_error_channel_delete_all);
  });
  
 $("#channel_messages_button").click(function(event) {
    log_channel("___messages___");
    channel_goto_messages(); 
 });

  
 

  $.fn.serializeChannel = function() {
    var o = {};
    var form_data = $('#channel_form').serializeArray();
    $.each(form_data, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }
  

  getNavbar("home", on_insert_navbar);
  try {request_channels();
  } catch (error) {
    error_channel("error request_channels");
  }
}


//TODO all fields for channel