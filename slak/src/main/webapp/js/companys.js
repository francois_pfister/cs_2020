/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/

/*---------- JS Controler for Company  --------------*/

var company_example = {
  name: "Foo"
}

var current_company;

const companys_uri = "rest/companys";

function log_company(m) {
  $('#companyMsgs').append($('<span class="success">' + m + '</span><br>'));
}

function error_company(m) {
  $('#companyMsgs').append($('<span class="invalid">' + m + '</span><br>'));
}

function handle_company_error(error, mesg) {
  if ((error.status == 409) || (error.status == 400)) {
    error_company(mesg);
    var errorMsg = $.parseJSON(error.responseText);
    $.each(errorMsg, function(index, err) {
      document.getElementById("msg_" + index).innerHTML = err;
    });
  } else {
    error_company(error.status + ": " + error.statusText);
  }
}

function handle_company_success(company, mesg) {
  current_company = company;
  $('#company_form')[0].reset();
  log_company(mesg);
  request_companys();
}

function handle_company_list_success(companys) {
    $("#companytable").empty().append(update_company_view(companys));
}

function on_success_company_update(company) {
  handle_company_success(company, "Company Updated");
}

function on_success_company_add(company) {
  handle_company_success(company, "Company Added");
}

function on_success_company_delete_byid(company) {
  handle_company_success(undefined, "Company Deleted");
}

function on_error_company_delete_all(error) {
  handle_company_error(error,"error while deleting all Companys!");
}

function on_error_company_delete_byid(error) {
 handle_company_error(error,"error while deleting Company!");
}

function on_error_company_update(error) {
  handle_company_error(error,"error while updating Company!");
}

function on_error_company_add(error) {
  handle_company_error(error,"error while adding Company!");
}

function on_error_companys(error) {
     error_company("error request Companys -" + error.status);
}

function request_companys() {
  request_entities(companys_uri, handle_company_list_success, on_error_companys);
}

function update_company_view(companys) {
  if (current_company == undefined)
    current_company = companys[0]
  on_success_company_form(current_company);
  return _.template($("#company-template").html(), {
    "companys": companys
  });
}

function on_success_company_delete_all(company) {
  current_company = undefined;
  $('#company_form')[0].reset();
  log_company("All Companys Deleted");
  request_companys();
}

function on_success_company_form(company) {
  try {
    //$('#name').val(company.name);
    //$('#email').val(company.email);
    current_company = company;
    $('#company_form').values(company);
  } catch (error) {
    error_company("error on_success_company_form");
  }
}

function on_error_company_form(err) {
  error_company(err.status + ": " + err.statusText);
}

function company_find(id) {
  request_entity(companys_uri + "/" + id,on_success_company_form, on_error_company_form);
}

function company_delete(id) {
  delete_by_id(companys_uri + "/" + id, on_success_company_delete_byid, on_error_company_delete_byid);
}

function clear_company_errors() {
  $('#companylayout').find("span.form_err").each(function(index) {
    $(this)[0].innerHTML = "";
  });
}

function init_company_page() {
  log_company("___init_company_page___");
  clear_company_errors();

  $('#company_form').submit(function(event) {
    var btn = document.activeElement.getAttribute('id');
    if (btn == "add")
      $('#id').val(-1);
    log_company("___company___" + btn);
    event.preventDefault();
    var companyData = $(this).serializeCompany();
    log_company("submit company : " + JSON.stringify(companyData));
    clear_company_errors();
    if (btn == "add")
      update_entity(companys_uri, "POST", companyData, on_success_company_add, on_error_company_add);
    else
      update_entity(companys_uri, "PUT", companyData, on_success_company_update, on_error_company_update);
  });

  $("#company_refresh_button").click(function(event) {
    log_company("___refresh___");
    request_companys();
  });

  $("#company_delete_button").click(function(event) {
    log_company("___delete___all");
    delete_all(companys_uri + "/all", on_success_company_delete_all, on_error_company_delete_all);
  });

  $.fn.serializeCompany = function() {
    var o = {};
    var form_data = $('#company_form').serializeArray();
    $.each(form_data, function() {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  }
  getNavbar("home", on_insert_navbar);
  try {
    request_companys();
  } catch (error) {
    error_company("error request_companys");
  }
}