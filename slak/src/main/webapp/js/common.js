
const DEFAULT_HOSTURI = "/slak";
//const ip_base ="127.0.0.1"; //TODO remove absolute uri
//const ip_base__ ="164.132.226.205";
//deploy_maya vps 2


//const host_base = "http://"+ip_base+":8080" + DEFAULT_HOSTURI;


//const helo_uri = host_base + "/helo";
const helo_uri = "helo";

const rest_base = "rest/";
const users_uri = rest_base + "users";
const invitations_uri = rest_base + "invitations";
const channels_uri = rest_base + "channels";
const messages_uri = rest_base + "messages";
const message_bchannels_uri = rest_base + "message_bchannels";


function request_lists(){
     request_entities(channels_uri, function(channels){ window.sessionStorage.setItem("channels", JSON.stringify(channels))});
}

function append_to_head(elemntType, content) {
	// detect whether provided content is "link" (instead of inline codes)
	var Is_Link = content.split(/\r\n|\r|\n/).length <= 1 && content.indexOf("//") > -1 && content.indexOf(" ") <= -1;
	if (Is_Link) {
		if (elemntType == 'script') { var x = document.createElement('script'); x.id = id; x.src = content; x.type = 'text/javascript'; }
		else if (elemntType == 'style') { var x = document.createElement('link'); x.id = id; x.href = content; x.type = 'text/css'; x.rel = 'stylesheet'; }
	}
	else {
		var x = document.createElement(elemntType);
		if (elemntType == 'script') { x.type = 'text/javascript'; x.innerHTML = content; }
		else if (elemntType == 'style') { x.type = 'text/css'; if (x.styleSheet) { x.styleSheet.cssText = content; } else { x.appendChild(document.createTextNode(content)); } }
	}
	//append in head
	(document.head || document.getElementsByTagName('head')[0]).appendChild(x);
}



function cerror(m) {
	console.error(m);
}


function req_navbar(n, cb) {
	return $.ajax({
		type: "Get",
		url: "rest/fragments/navbar/" + n,
		success: function(response) {
			cb(response);
		},
		error: function(req, status, err) {
			console.log('something went wrong for req_navbar:', status, err);
		}
	});
}


function request_helo(call_success, call_err) {
	return $.ajax({
		type: "Get",
		url: helo_uri,
		success: function(response) {
			console.log("request_helo=" + response);
			call_success(response);
		},
		error: function(req, status, err) {
			call_err(err);
		}
	});
}



function request_entity(uri, call_success, call_err, target) {
	return $.ajax({
		url: uri,
		dataType: 'json',
		type: "GET",
		success: function(response) {
			call_success(response, target);
		},
		error: function(err) {
			call_err(err, target);
		}
	});
}

function request_entities(uri, call_success, call_err) {
	return $.ajax({
		url: uri,
		cache: false,
		success: function(response) {
			call_success(response);
		},
		error: function(err) {
		   if (call_err != undefined)
			  call_err(err);
			else
			  console.log("error "+err);
		}
	});
}

function delete_by_id(uri, call_success, call_err) {
	return $.ajax({
		url: uri,
		type: "DELETE",
		success: function(response) {
			call_success(response);
		},
		error: function(err) {
			call_err(err);
		}
	});
}


function delete_all(uri, call_success, call_err) {
	return $.ajax({
		url: uri,
		type: "DELETE",
		success: function(response) {
			call_success(response);
		},
		error: function(err) {
			call_err(err);
		}
	});
}


function update_entity(uri, verb, entity, call_success, call_err) {
	return $.ajax({
		url: uri,
		contentType: "application/json",
		dataType: "json",
		type: verb,
		data: JSON.stringify(entity),
		success: function(response) {
			call_success(response);
		},
		error: function(err) {
			call_err(err);
		}
	});
}


function getNavbar(n, cb) {
	var nb = sessionStorage.getItem("navbar_" + n);
	if (nb == undefined) {
		try {
			req_navbar(n, cb);
			sessionStorage.getItem("navbar_" + n, nb);
		} catch (error) {
			cb("no navbar");
		}
	} else
		cb(nb);
}


function on_insert_navbar(content) {
	document.getElementById("navbar").innerHTML = content;
}


