/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/
package org.connecthive.app.model;
  

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/*---------- generated  Message --------------*/
@Entity
@NamedQueries(
{ 
   @NamedQuery(name = "allMessagesByUser", query = "select a FROM Message a where a.emitter.id = :user_id"),
   @NamedQuery(name = "allMessagesByChannel", query = "select a FROM Message a where a.channel.id = :channel_id")
})
@Table(name="Message")
public class Message implements java.io.Serializable{
	
	private static final String pattern = "yyyy-MM-dd";
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);


    @Id
    @GeneratedValue
    private Integer id;
    private int parentId;
    private String content;
    private String type;
    private Date date;
    @ManyToOne
    //@JsonBackReference(value="emitter_b")
    @JoinColumn(name = "emitter_fk")
    private User emitter;
    @ManyToOne
    //@JsonBackReference(value="channel_b")
    @JoinColumn(name = "channel_fk")
    private Channel channel;

    
   
    
    
    public void setId(Integer id){
       this.id = id;
    }
    
    public Integer getId(){
        return id;
    }

    public void setParentId(int parentId){
       this.parentId = parentId;
    }
    
    public int getParentId(){
        return parentId;
    }

    public void setContent(String content){
       this.content = content;
    }
    
    public String getContent(){
        return content;
    }

    public void setType(String type){
       this.type = type;
    }
    
    public String getType(){
        return type;
    }

	@JsonIgnore
    public void setDate(Date date){
       this.date = date;
    }
    
	@JsonIgnore
    public Date getDate(){
        return date;
    }

    public void setEmitter(User emitter){
       this.emitter = emitter;
    }
    
    public User getEmitter(){
        return emitter;
    }

    public void setChannel(Channel channel){
       this.channel = channel;
    }
    
    public Channel getChannel(){
        return channel;
    }
    
	public String getDateString() {
		if (date!=null)
		    return simpleDateFormat.format(date);
		else
			return "";
	}

	
	public void setDateString(String dateString) {
		Date date;
		try {
			date = simpleDateFormat.parse(dateString);
			setDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	}    
}
//TODO all fields for messages
//TODO remove JsonBackReference from message