/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/
package org.connecthive.app.model;
  

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

/*---------- generated  Channel --------------*/
@Entity
@Table(name="Channel")
public class Channel implements java.io.Serializable{
	
	
	private static final String pattern = "yyyy-MM-dd";

	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String description;
    private boolean privat;
    private Date dateCreation;
  


	public void setId(Integer id){
       this.id = id;
    }
    
    public Integer getId(){
        return id;
    }

    public void setName(String name){
       this.name = name;
    }
    
    public String getName(){
        return name;
    }

    public void setDescription(String description){
       this.description = description;
    }
    
    public String getDescription(){
        return description;
    }

    public void setPrivat(boolean privat){
       this.privat = privat;
    }
    
    public boolean getPrivat(){
        return privat;
    }

	@JsonIgnore
    public void setDateCreation(Date dateCreation){
       this.dateCreation = dateCreation;
    }
    
	@JsonIgnore
    public Date getDateCreation(){
        return dateCreation;
    }
    
	public String getDateCreationString() {
		if (dateCreation!=null)
		    return simpleDateFormat.format(dateCreation);
		else
			return "";
	}

	
	public void setDateCreationString(String dateCreationString) {
		Date date;
		try {
			date = simpleDateFormat.parse(dateCreationString);
			setDateCreation(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}	
	} 
}