/**************************************************
 * Copyright (c) 2020 Connecthive
 * all rights reserved
 * Created by pfister on 2020-11-23 thanks to freemarker
 ***************************************************/
package org.connecthive.app.model;
  

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/*---------- generated  Company --------------*/
@Entity
@Table(name="Company")
public class Company implements java.io.Serializable{

    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private String website;

    public void setId(Integer id){
       this.id = id;
    }
    
    public Integer getId(){
        return id;
    }

    public void setName(String name){
       this.name = name;
    }
    
    public String getName(){
        return name;
    }

    public void setWebsite(String website){
       this.website = website;
    }
    
    public String getWebsite(){
        return website;
    }
}